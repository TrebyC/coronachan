import os
import discord
import praw
import information
import urllib.request
from discord.ext import commands

with open('../official.txt') as extra:
    extra = extra.read().split(',')
    token = extra[1]
    prefix = extra[0]
    client_id=extra[2]
    client_secret=extra[3]
    user_agent=extra[4]

bot = commands.Bot(command_prefix=prefix,alias='coronachan')

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))

@bot.command()
async def invite(ctx):
    await ctx.send('Invite this Bot to your server!\nhttps://discordapp.com/oauth2/authorize?client_id=694316585824944229&scope=bot&permissions=59392')

@bot.command(alias='chan')
async def coronachan(ctx):
    "NSFW command. Pulls random posts from r/coronachan"
    if not ctx.message.channel.is_nsfw():
        await ctx.send('Please use this command in a NSFW-Channel.')
        return
    reddit = praw.Reddit(client_id=client_id,client_secret=client_secret,user_agent='testscript by /u/VTrebyC')
    chan = reddit.subreddit('coronachan').random()
    if('youtu' in chan.url):
        await ctx.send(chan.url)
    else:
        if 'png' in chan.url:
            urllib.request.urlretrieve(chan.url, 'chan.png')
            await ctx.send(file=discord.File('chan.png'))
            os.remove('chan.png')
        elif 'jpeg' in chan.url:
            urllib.request.urlretrieve(chan.url, 'chan.jpeg')
            await ctx.send(file=discord.File('chan.jpeg'))
            os.remove('chan.jpeg')
        elif 'jpg' in chan.url:
            urllib.request.urlretrieve(chan.url, 'chan.jpg')
            await ctx.send(file=discord.File('chan.jpg'))
            os.remove('chan.jpg')


@bot.command()
async def logout(ctx):
    if ctx.author.id == 153226548835516417:
        await bot.logout()
    else:
        await ctx.send('**NICE TRY**')

@bot.command()
async def total(ctx, *args):
    "total [country] (Days after 22nd of January of 2020)"
    if len(args) ==1:
        await ctx.send(information.total(args[0]))
    else:
        try:
            await ctx.send(information.total1(args[0], int(args[1])))
        except:
            pass

@bot.command(name='percentage')
async def percentage(ctx, *args):
    "percentage [country]"
    percentage = (float(information.total(args[0]))/float(information.total('world')))*100
    await ctx.send(args[0] + " current percentage of total cases is "+ str(percentage) + '%')

@bot.command(name='active')
async def active(ctx, *args):
    "active [country] (Days after 22nd of January of 2020)"
    if len(args) ==1:
        await ctx.send(information.active(args[0]))
    else:
        try:
            await ctx.send(information.active1(args[0], int(args[1])))
        except:
            pass

@bot.command(name='activepercentage')
async def apercenatage(ctx, *args):
    "activepercentage [country]"
    percentage = (float(information.active(args[0]))/float(information.active('world')))*100
    await ctx.send(args[0] + " current percentage of active cases is "+ str(percentage) + '%')

@bot.command(name='deaths')
async def deaths(ctx, *args):
    "deaths [country] (Days after 22nd of January of 2020)"
    if len(args) ==1:
        await ctx.send(information.deaths(args[0]))
    else:
        try:
            await ctx.send(information.deaths1(args[0], int(args[1])))
        except:
            pass

@bot.command(name='deadpercentage')
async def deadpercenatage(ctx, *args):
    "deadpercentage"
    percentage = (float(information.deaths(args[0]))/float(information.deaths('world')))*100
    await ctx.send(args[0] + " current percentage of deaths is "+ str(percentage) + '%')

@bot.command()
async def graph(ctx, *args):
    "graph [stat] [country 1] [country 2] ... [country n]"
    information.chart(args[0], args[1:])
    await ctx.send(file=discord.File('graph.png'))
    os.remove('graph.png')

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    await bot.process_commands(message)




bot.run(token)