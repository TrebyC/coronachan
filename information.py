import json
import os
import time
import matplotlib.pyplot

def getData():
    # #4 times a day, because i'm lazy
    try:
        os.remove('timeseries.json')
    except:
        pass
    os.system('wget https://pomber.github.io/covid19/timeseries.json')
    with open('timeseries.json') as d:
        data = json.load(d)
    return data

#TODO: add Percentage of Population

def total(country):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += data[cou[i]][len(data[cou[i]])-1]['confirmed']
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][len(data[c[0]])-1]['confirmed']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][len(data[c[0]]) - 1]['confirmed']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][len(data[c[0]]) - 1]['confirmed']

def total1(country, date):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += data[cou[i]][date]['confirmed']
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][date]['confirmed']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][date]['confirmed']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][date]['confirmed']

def active(country):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += (data[cou[i]][len(data[cou[i]])-1]['confirmed'] - data[cou[i]][len(data[cou[i]])-1]['deaths'] - data[cou[i]][len(data[cou[i]])-1]['recovered'])
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][len(data[c[0]])-1]['confirmed'] - data[c[0]][len(data[c[0]])-1]['deaths'] - data[c[0]][len(data[c[0]])-1]['recovered']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][len(data[c[0]]) - 1]['confirmed'] - data[c[0]][len(data[c[0]])-1]['deaths'] - data[c[0]][len(data[c[0]])-1]['recovered']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][len(data[c[0]]) - 1]['confirmed'] - data[c[0]][len(data[c[0]])-1]['deaths'] - data[c[0]][len(data[c[0]])-1]['recovered']

def active1(country,date):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += (data[cou[i]][date]['confirmed'] - data[cou[i]][date]['deaths'] - data[cou[i]][date]['recovered'])
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][date]['confirmed'] - data[c[0]][date]['deaths'] - data[c[0]][date]['recovered']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][date]['confirmed'] - data[c[0]][date]['deaths'] - data[c[0]][date]['recovered']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][date]['confirmed'] - data[c[0]][date]['deaths'] - data[c[0]][date]['recovered']


def deaths(country):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += data[cou[i]][len(data[cou[i]])-1]['deaths']
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][len(data[c[0]])-1]['deaths']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][len(data[c[0]]) - 1]['deaths']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][len(data[c[0]]) - 1]['deaths']

def deaths1(country, date):
    data = getData()
    with open('countries.json') as c:
        countries = json.load(c)
    cou = list(data)
    if 'earth' in country.lower() or 'world' in country.lower():
        out = 0
        for i in range(len(data)):
            out += data[cou[i]][date]['deaths']
        return out
    else:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == country.casefold():
                    return data[c[0]][date]['deaths']
            except:
                print('')

            try:
                if(c[1]['flag'].casefold() == country.casefold()):
                    return data[c[0]][date]['deaths']
            except:
                print('')

            if c[0].casefold() == country.casefold():
                return data[c[0]][date]['deaths']


def chart(stat, country):
    plt = matplotlib.pyplot
    plt.clf()
    data = getData()
    statCountries = []
    with open('countries.json') as c:
        countries = json.load(c)
    
    for v in country:
        for c in countries.items():
            try:
                if c[1]['code'].casefold() == v.casefold():
                    statCountries.append(c[0])
            except:
                print('')

            try:
                if (c[1]['flag'].casefold() == v.casefold()):
                    statCountries.append(c[0])
            except:
                print('')

            if c[0].casefold() == v.casefold():
                statCountries.append(c[0])

    plt.style.use('bmh')

    if(stat=='new'):
        # x axis values
        x = []
        # corresponding y axis values
        y = []
        n = 1
        c = statCountries[0]
        for i in range(len(data[c])-1):
            x.append(n)
            y.append(data[c][i+1]['confirmed']-data[c][i]['confirmed'])
            n+=1
        # plotting the points
        plt.bar(x, y, label=c)
        plt.text(n, y[len(y) - 1], str(y[len(y) - 1]))

        # naming the x axis
    plt.xlabel('Days since Jan 22, 2020')
    # naming the y axis
    plt.ylabel('New Infections')

    plt.title('New Infections')

    if(stat=='deaths'):

        for c in statCountries:

            # x axis values
            x = []
            # corresponding y axis values
            y = []
            n = 1
            for i in range(len(data[c])):
                if data[c][i]['deaths'] > 100:
                    x.append(n)
                    y.append(data[c][i]['deaths'])
                    n+=1

            # plotting the points
            plt.plot(x, y, label=c)
            plt.text(n, y[len(y)-1], str(y[len(y)-1]))

        # naming the x axis
        plt.xlabel('Days since first 100 deaths')
        # naming the y axis
        plt.ylabel('Deaths')

        plt.title('Deaths')
    if(stat == 'total'):
        for c in statCountries:

            # x axis values
            x = []
            # corresponding y axis values
            y = []
            n = 1
            for i in range(len(data[c])):
                if data[c][i]['confirmed'] > 100:
                    x.append(n)
                    y.append(data[c][i]['confirmed'])
                    n+=1

            # plotting the points
            plt.plot(x, y, label=c)
            plt.text(n, y[len(y) - 1], str(y[len(y) - 1]))
        # naming the x axis
        plt.xlabel('Days since first 100 cases')
        # naming the y axis
        plt.ylabel('Cases')

        plt.title('Total confirmed cases')

    if(stat == 'active'):
        for c in statCountries:

            # x axis values
            x = []
            # corresponding y axis values
            y = []
            n = 1
            for i in range(len(data[c])):
                if data[c][i]['confirmed'] > 100:
                    x.append(n)
                    y.append(data[c][i]['confirmed']-data[c][i]['recovered']-data[c][i]['deaths'])
                    n+=1

            # plotting the points
            plt.plot(x, y, label=c)
            plt.text(n, y[len(y) - 1], str(y[len(y) - 1]))

        # naming the x axis
        plt.xlabel('Days since first 100 cases')
        # naming the y axis
        plt.ylabel('Cases')

        plt.title('Active cases')


    plt.legend(loc='upper left')
    graph = plt.savefig('graph.png')

    # function to show the plot
